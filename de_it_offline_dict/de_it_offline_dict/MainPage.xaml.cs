﻿using de_it_offline_dict.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

namespace de_it_offline_dict
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            GerItDictionaryLoader.Load();
        }

        private void OnInputTextBoxKeyDown(object sender, KeyRoutedEventArgs e)
        {
            Search();
        }

        private void Search()
        {
            ResultPresenterListView.ItemsSource = null;
            var query = InputTextBox.Text;
            var searchResults = GerItDictionary.Instance.SearchFor(query);
            if (searchResults.Count < 100)
            {
                ResultPresenterListView.ItemsSource = searchResults;
            }
        }

        private void OnSearchButtonClick(object sender, TappedRoutedEventArgs e)
        {
            Search();
        }


    }
}
