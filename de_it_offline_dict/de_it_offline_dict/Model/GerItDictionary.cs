﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace de_it_offline_dict.Model
{
    public class GerItDictionary
    {
        private List<GerItDictionaryEntry> _entries;
        public List<GerItDictionaryEntry> Entries
        {
            get { return _entries ?? (_entries = new List<GerItDictionaryEntry>()); }
        }

        private static GerItDictionary _instance;
        public static GerItDictionary Instance
        {
            get 
            {
                if (_instance == null)
                {
                    _instance = new GerItDictionary();
                }
                return _instance;
            }
        }

        public void AddEntry(GerItDictionaryEntry entry)
        {
            // check german word
            Entries.Add(entry);
            
        }

        internal void Add(List<GerItDictionaryEntry> list)
        {
            foreach (var e in list)
            {
                AddEntry(e);
            }
        }

        internal List<GerItDictionaryEntry> SearchFor(string query)
        {
            var result = new List<GerItDictionaryEntry>();

            foreach (var e in Entries)
            {
                if (e.GerWord.ToLower().Contains(query.ToLower()) ||
                    e.ItWord.ToLower().Contains(query.ToLower()))
                {
                    result.Add(e);
                }
            }

            return result;
        }
    }
}
