﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Windows.Storage;

namespace de_it_offline_dict.Model
{
    public class GerItDictionaryLoader
    {
        public static async void Load()
        {
            var omegaWikiFile = await StorageFile.GetFileFromApplicationUriAsync(new Uri("ms-appx:///Assets/DictResources/omegawiki_de_it.txt"));
            var omegaWikiText = await FileIO.ReadTextAsync(omegaWikiFile);

            var wikipediaFile = await StorageFile.GetFileFromApplicationUriAsync(new Uri("ms-appx:///Assets/DictResources/wikipedia_de_it.txt"));
            var wikipediaText = await FileIO.ReadTextAsync(omegaWikiFile);

            var wiktionaryFile = await StorageFile.GetFileFromApplicationUriAsync(new Uri("ms-appx:///Assets/DictResources/wiktionary_de_it.txt"));
            var wiktionaryText = await FileIO.ReadTextAsync(wiktionaryFile);

            await Task.Factory.StartNew(() =>
            {
                omegaWikiText = RemoveComments(omegaWikiText);
                wikipediaText = RemoveComments(wikipediaText);
                wiktionaryText = RemoveComments(wiktionaryText);

                GerItDictionary.Instance.Add(ParseDictionary(omegaWikiText));
                GerItDictionary.Instance.Add(ParseDictionary(wikipediaText));
                GerItDictionary.Instance.Add(ParseDictionary(wiktionaryText));

                GerItDictionary.Instance.Entries.Sort((a, b) => { return a.GerWord.CompareTo(b.GerWord); });

                // remove duplicates
                for (int i = 1; i < GerItDictionary.Instance.Entries.Count; i++)
                {
                    if (GerItDictionary.Instance.Entries[i].GerWord.ToLower().Equals(GerItDictionary.Instance.Entries[i - 1].GerWord.ToLower()))
                    {
                        GerItDictionary.Instance.Entries.RemoveAt(i);
                    }
                }
            });

        }

        private static string RemoveComments(string text)
        {
            var commentRegex = new Regex(@"#.*?\n");
            foreach (var m in commentRegex.Matches(text))
            {
                var comment = m.ToString();
                text = text.Replace(comment, "");
            }
            return text;
        }

        private static List<GerItDictionaryEntry> ParseDictionary(string text)
        {
            var result = new List<GerItDictionaryEntry>();

            var regex = new Regex(@"(?<ger>.*?)\t(?<it>.*?)\n");
            var mc = regex.Matches(text);
            foreach (Match m in mc)
            {
                result.Add(new GerItDictionaryEntry(m.Groups["ger"].ToString(), m.Groups["it"].ToString()));
            }

            return result;
        }
    }
}
