﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace de_it_offline_dict.Model
{
    public class GerItDictionaryEntry
    {
        public String GerWord { get; private set; }
        public String ItWord { get; private set; }

        public GerItDictionaryEntry(string ger, string it)
        {
            GerWord = ger;
            ItWord = it;
        }
    }
}
